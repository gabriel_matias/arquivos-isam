package br.com.pra.isam;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TratadorDeArquivoTest {
	
	TratadorDeArquivo tratador;
	String caminhoDoPrimeiroArquivo;
	String caminhoDoSegundoArquivo;
	String caminhoDoTerceiroArquivo;
	int numeroDeColunasDoPrimeiroArquivo;
	int numeroDeColunasDoSegundoArquivo;
	int numeroDeColunasDoTerceiroArquivo;
	int tamanhoMaximoDaColunaA;
	int tamanhoMaximoDaColunaB;
	int tamanhoMaximoDaColunaC;
	int tamanhoMaximoDaColunaD;
	
	@Before
	public void setUp() throws Exception {
		tratador = new TratadorDeArquivo();
		caminhoDoPrimeiroArquivo = new String("arquivos/returned.csv");
		caminhoDoSegundoArquivo = new String("arquivos/Sample - Superstore Sales.csv");
		caminhoDoTerceiroArquivo = new String("arquivos/users.csv");
		numeroDeColunasDoPrimeiroArquivo = tratador.contaColunas(caminhoDoPrimeiroArquivo);
		numeroDeColunasDoSegundoArquivo = tratador.contaColunas(caminhoDoSegundoArquivo);
		numeroDeColunasDoTerceiroArquivo = tratador.contaColunas(caminhoDoTerceiroArquivo);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void deveAcertarNumeroDeColunasDosTresArquivos() throws FileNotFoundException {
		assertEquals(2, numeroDeColunasDoPrimeiroArquivo);
		assertEquals(21, numeroDeColunasDoSegundoArquivo);
		assertEquals(2, numeroDeColunasDoTerceiroArquivo);
	}
	
	@Test
	public void deveAcertarOTamanhoMaximoDasColunasDoArquivoReturned() throws FileNotFoundException {
		tamanhoMaximoDaColunaA = (int) tratador.calculaTamanhoMaximoDeCadaColuna(caminhoDoPrimeiroArquivo, numeroDeColunasDoPrimeiroArquivo).get(0);
		tamanhoMaximoDaColunaB = (int) tratador.calculaTamanhoMaximoDeCadaColuna(caminhoDoPrimeiroArquivo, numeroDeColunasDoPrimeiroArquivo).get(1);
		assertEquals(8, tamanhoMaximoDaColunaA);
		assertEquals(8, tamanhoMaximoDaColunaB);
	}
	
	@Test
	public void deveAcertarOTamanhoMaximoDasColunasDoArquivoSample() throws FileNotFoundException {
		tamanhoMaximoDaColunaD = (int) tratador.calculaTamanhoMaximoDeCadaColuna(caminhoDoSegundoArquivo, numeroDeColunasDoSegundoArquivo).get(20);
		tamanhoMaximoDaColunaA = (int) tratador.calculaTamanhoMaximoDeCadaColuna(caminhoDoSegundoArquivo, numeroDeColunasDoSegundoArquivo).get(0);
		tamanhoMaximoDaColunaB = (int) tratador.calculaTamanhoMaximoDeCadaColuna(caminhoDoSegundoArquivo, numeroDeColunasDoSegundoArquivo).get(11);
		tamanhoMaximoDaColunaC = (int) tratador.calculaTamanhoMaximoDeCadaColuna(caminhoDoSegundoArquivo, numeroDeColunasDoSegundoArquivo).get(14);
		
		assertEquals(6, tamanhoMaximoDaColunaA);
		assertEquals(22, tamanhoMaximoDaColunaB);
		assertEquals(16, tamanhoMaximoDaColunaC);
		assertEquals(10, tamanhoMaximoDaColunaD);
	}
	
	@Test
	public void deveAcertarOTamanhoMaximoDasColunasDoArquivoUsers() throws FileNotFoundException {
		tamanhoMaximoDaColunaA = (int) tratador.calculaTamanhoMaximoDeCadaColuna(caminhoDoTerceiroArquivo, numeroDeColunasDoTerceiroArquivo).get(0);
		tamanhoMaximoDaColunaB = (int) tratador.calculaTamanhoMaximoDeCadaColuna(caminhoDoTerceiroArquivo, numeroDeColunasDoTerceiroArquivo).get(1);
		assertEquals(7, tamanhoMaximoDaColunaA);
		assertEquals(7, tamanhoMaximoDaColunaB);
	}
	
	@Test
	public void deveCriarOsArquivosComEspacoEmBranco() throws FileNotFoundException {
		tratador.igualaColunas(caminhoDoPrimeiroArquivo);
		tratador.igualaColunas(caminhoDoSegundoArquivo);
		tratador.igualaColunas(caminhoDoTerceiroArquivo);
	}
}
