package br.com.pra.isam;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * 
 * Classe responsavel por igualar as colunas dos campos dos arquivos conforme a maior coluna encontrada
 *
 */
public class TratadorDeArquivo {
	
	/**
	 * Metodo para igualar as colunas dos campos dos arquivos conforme a maior coluna encontrada
	 * @param caminhoDoArquivo
	 * @throws FileNotFoundException
	 */
	public void igualaColunas(String caminhoDoArquivo) throws FileNotFoundException {
		
		int numeroDeColunas = this.contaColunas(caminhoDoArquivo);
		
		List<Integer> tamanhoDeCadaColuna = this.calculaTamanhoMaximoDeCadaColuna(caminhoDoArquivo, numeroDeColunas);
		
		this.adicionaEspacos(caminhoDoArquivo, tamanhoDeCadaColuna);
		
	}
	
	/**
	 * Metodo para contar quantas colunas o arquivo possui
	 * @param caminhoDoArquivo
	 * @return
	 * @throws FileNotFoundException
	 */
	public int contaColunas(String caminhoDoArquivo) throws FileNotFoundException {
		
		Scanner leitor = null;
		int numeroDeColunas = 0;
		
		try {
			leitor = new Scanner(new FileInputStream(caminhoDoArquivo));
			
			String linha = leitor.nextLine();
		    StringTokenizer tokenizer = separaPalavras(linha);
		    numeroDeColunas = tokenizer.countTokens();
		}
		finally {
			if (leitor != null) leitor.close();
		}
		
		return numeroDeColunas;
	}
	
	/**
	 * Calcula o maior tamanho de cada coluna do arquivo
	 * @param caminhoDoArquivo
	 * @param numeroDeColunas
	 * @return
	 * @throws FileNotFoundException
	 */
	public List<Integer> calculaTamanhoMaximoDeCadaColuna(String caminhoDoArquivo, int numeroDeColunas) throws FileNotFoundException {
		
		Scanner leitor = null;
		List<Integer> tamanhoMaximoDeCadaColuna = new ArrayList<Integer>();
		
		try {
			leitor = new Scanner(new FileInputStream(caminhoDoArquivo));
		       
		    for (int i = 0; i < numeroDeColunas; i++) {
		       tamanhoMaximoDeCadaColuna.add(0); 
		    }
		    
		    int coluna = 0;
		    
		    String linhaAtual;
		    String palavra;
		    StringTokenizer tokenizador;
		    
	        while (leitor.hasNextLine()) {
	    	   linhaAtual = leitor.nextLine();
	    	   tokenizador = this.separaPalavras(linhaAtual);
	    	   
	    	   while(tokenizador.hasMoreTokens() && coluna < numeroDeColunas) {
	    		   palavra = tokenizador.nextToken();
	    		   if(tamanhoMaximoDeCadaColuna.get(coluna).compareTo(palavra.length()) == -1){
			 		   tamanhoMaximoDeCadaColuna.set(coluna, palavra.length());
		 		   }
	    		   coluna++;
	    	   }
	    	   
	    	   coluna = 0;
	        }
			
		}
		finally {
			if (leitor != null) leitor.close();
		}		
		
		return tamanhoMaximoDeCadaColuna;
	}
	
	private StringTokenizer separaPalavras(String linha) {
		StringTokenizer palavras = new StringTokenizer(linha, ";");
		return palavras;
	}
	
	/**
	 * Adiciona espacos em branco em cada campo para igualar as colunas
	 * @param caminhoDoArquivo
	 * @param tamanhoMaximoDeCadaColuna
	 * @throws FileNotFoundException
	 */
	private void adicionaEspacos(String caminhoDoArquivo, List<Integer> tamanhoMaximoDeCadaColuna) throws FileNotFoundException {
		
		Scanner leitor = null;
		PrintWriter escritor = null;
		
		String caminhoDaSaida = caminhoDoArquivo.substring(0, caminhoDoArquivo.length()-4);
		
		try {
			
			leitor = new Scanner(new FileInputStream(caminhoDoArquivo));
			escritor = new PrintWriter(new OutputStreamWriter(
				      new BufferedOutputStream(new FileOutputStream(caminhoDaSaida+".pra"))));
			
			int coluna = 0;
			String linhaAtual;
			StringTokenizer tokenizador;
			String palavra;
			
			while(leitor.hasNextLine()){
				
				linhaAtual = leitor.nextLine();
		    	tokenizador = this.separaPalavras(linhaAtual);
		    	   
		    	while(tokenizador.hasMoreTokens() && coluna < tamanhoMaximoDeCadaColuna.size()) {
		    		
		    		palavra = tokenizador.nextToken();
		    		
		    		escritor.print(String.format("%" + -((int)tamanhoMaximoDeCadaColuna.get(coluna)) + "s", palavra));
		    	
			 		coluna++;
		    		if(tokenizador.hasMoreTokens()){
		    			escritor.print(";");
		    		}
		    		else{
		    			escritor.println("");
		    		}
		    	}		   
		    	   
		    	coluna = 0;
			}
			
		}
		finally {
			if (leitor != null) leitor.close();
			if(escritor != null) {
			    escritor.flush();
			    escritor.close();
			}
		}
	}
	
}